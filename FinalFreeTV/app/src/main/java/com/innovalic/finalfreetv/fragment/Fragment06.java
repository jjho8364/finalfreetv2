package com.innovalic.finalfreetv.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.innovalic.finalfreetv.R;
import com.innovalic.finalfreetv.activity.MainActivity;
import com.innovalic.finalfreetv.activity.VideoListActivity;
import com.innovalic.finalfreetv.activity.VideoViewActivity;
import com.innovalic.finalfreetv.adapter.Fr1GridViewAdapter;
import com.innovalic.finalfreetv.adapter.GridViewAdapter;
import com.innovalic.finalfreetv.model.Fr1GridViewItem;
import com.innovalic.finalfreetv.model.GridViewItem;
import com.innovalic.finalfreetv.utils.NetworkUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-02.
 */
public class Fragment06 extends Fragment implements View.OnClickListener {
    private final String TAG = " Fragment06 - ";
    private ImageView sbs_ori;
    private GridView gridView;
    private Fr1GridViewAdapter adapter;

    public Fragment06() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        ((MainActivity)getActivity()).hideHistoryBtn(); // history hide
        int portrait_width_pixel= Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        //boolean phoneNumFlag = false;
        boolean usimFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        int contactsCnt = c.getCount();
        if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        //String phoneNum = telManager.getLine1Number();
        //if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        //if(operator == null || operator.equals("")){ operatorFlag = true; }
        if (telManager.getSimState() == TelephonyManager.SIM_STATE_ABSENT){
            Log.d(TAG, "유심 x");
            usimFlag = true;
        } else {
            Log.d(TAG, "유심 o");
            usimFlag = false;
        }

        Log.d(TAG, "contactsFlag : " + contactsFlag);
        Log.d(TAG, "usimFlag : " + usimFlag);
        Log.d(TAG, "isPhone : " + isPhone);
        //Log.d(TAG, "phoneNumFlag : " + phoneNumFlag);
        if(false){
        //if ((contactsFlag || usimFlag) && isPhone) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment06, container, false);

            if(NetworkUtil.getConnectivity(getActivity())){
                final ArrayList<Fr1GridViewItem> gridArr = new ArrayList<Fr1GridViewItem>();
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/kbs1-banner.jpg", "http://www.sinktv.com/ch/korea/kbs1.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/kbs2-banner.jpg", "http://www.sinktv.com/ch/korea/kbs2.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/kbs2-banner.jpg", "http://www.sinktv.com/ch/korea/kbs2-2.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/mbc-banner.jpg", "http://www.sinktv.com/ch/korea/mbc-2.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/mbc-banner.jpg", "http://www.sinktv.com/ch/korea/mbc.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/sbs-banner.jpg", "http://www.sinktv.com/ch/korea/sbs.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/sbs-banner.jpg", "http://www.sinktv.com/ch/korea/sbs-2.php"));
                //gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2014/03/jtbc-banner.jpg", "http://www.sinktv.com/ch/korea/jtbc.php"));
                //gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2014/03/channela-banner.jpg", "http://www.sinktv.com/ch/korea/channela.php"));
                // kbs2
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/kbs2-banner.jpg", "https://aqstream.com/kbs2/KBS2-Stream-4"));
                // mbc
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/mbc-banner.jpg", "https://aqstream.com/mbc/MBC-Stream-5"));
                // sbs
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/sbs-banner.jpg", "https://aqstream.com/sbs/SBS-Stream-5"));
                // ogn
                gridArr.add(new Fr1GridViewItem("http://img.lifestyler.co.kr/uploads/program/1/1102/skin/f130821426772198575(0).png", "https://aqstream.com/ogn/OGN-Stream-2"));
                // tvn
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/tvn-banner.jpg", "https://aqstream.com/tvn/tvN-Mobile"));
                // jtbc
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2014/03/jtbc-banner.jpg", "https://aqstream.com/jtbc/JTBC-HD"));
                // mnet
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/mnet-banner.jpg", "https://aqstream.com/mnet/Mnet-Mobile"));
                // onstyle
                gridArr.add(new Fr1GridViewItem("https://tv.pstatic.net/thm?size=c53x53&quality=10&q=http://post.phinf.naver.net/20160627_235/itsonstyle_1467017635816GHgRt_JPEG/itsonstyle_7682564314683993317.jpg?type=f120_120", "https://aqstream.com/onstyle/OnStyle-Stream-2"));
                // olive
                gridArr.add(new Fr1GridViewItem("https://tv.pstatic.net/thm?size=c53x53&quality=10&q=http://post.phinf.naver.net/20160329_141/olive_post_1459231450194AioJG_JPEG/olive_post_6373326454529821086.jpg?type=f120_120", "https://aqstream.com/olive/O'live-Stream-2"));
                // xtm
                gridArr.add(new Fr1GridViewItem("https://tv.pstatic.net/thm?size=c53x53&quality=10&q=http://post.phinf.naver.net/20150831_112/xtm_tv_1441014914882Nd5KD_PNG/xtm_tv_5403547753951263790.png?type=f120_120", "https://aqstream.com/xtm/XTM-Stream-2"));
                // kbs drama
                gridArr.add(new Fr1GridViewItem("http://www.kbsn.co.kr/images/common/logo_drama.png", "https://aqstream.com/kbsd/KBS-Drama-Stream-3"));
                // kbs joy
                gridArr.add(new Fr1GridViewItem("http://www.kbsn.co.kr/images/common/logo_joy.png", "https://aqstream.com/kbsj/KBS-Joy-Stream-3"));
                // kbs n sports
                gridArr.add(new Fr1GridViewItem("http://www.kbsn.co.kr/images/common/logo_sports.png", "https://aqstream.com/kbsns/KBS-N-Sports-Mobile"));
                //kbs news 24
                gridArr.add(new Fr1GridViewItem("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/79/16/57_667916_poster_image_1417502523733.jpg", "https://aqstream.com/kbs24/KBS-24-News-Mobile"));
                // mbc 드라마넷
                gridArr.add(new Fr1GridViewItem("https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-1/c19.0.50.50/p50x50/13319839_787215218044729_115602438649387075_n.png?oh=2f90fd3c377691cfc569e7a59351d378&oe=5837CC1E", "https://aqstream.com/mbcd/MBC-Drama-Stream-3"));
                // mbc everyone
                gridArr.add(new Fr1GridViewItem("https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-1/p50x50/10270433_657663084304839_2551115337047960258_n.jpg?oh=4b62c33a32d77808d63025ce4037f2c0&oe=587E6028", "https://aqstream.com/mbce1/MBC-Every1-Stream-3"));
                // mbc music
                gridArr.add(new Fr1GridViewItem("https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-1/p50x50/10516699_985711421453997_6547383953568675538_n.jpg?oh=6ed9ceaa5d7ffd1c7473ca5061eb00a4&oe=5843FC7C", "https://aqstream.com/mbcm/MBC-Music-Stream-3"));
                // sbs plus
                gridArr.add(new Fr1GridViewItem("http://img.sbs.co.kr/sw11/gnb/common/logo_plus.jpg", "https://aqstream.com/sbsp/SBS-Plus-Stream-4"));
                // sbs fune
                gridArr.add(new Fr1GridViewItem("http://img.sbs.co.kr/sw11/gnb/common/logo_fune.jpg", "https://aqstream.com/sbsf/SBS-funE-Stream-4"));
                // sbs golf
                gridArr.add(new Fr1GridViewItem("http://img.sbs.co.kr/sw11/gnb/common/logo_golf.jpg", "https://aqstream.com/sbsg/SBS-Golf-Stream-4"));
                // sbs sports
                gridArr.add(new Fr1GridViewItem("http://img.sbs.co.kr/sw11/gnb/common/logo_sports.jpg", "https://aqstream.com/sbssp/SBS-Sports-Stream-2"));
                // sbs mtv
                gridArr.add(new Fr1GridViewItem("http://www.mtv.co.kr/gsp/sbs-mtv-footer-logo.jpg", "https://aqstream.com/sbsmtv/SBS-MTV-Mobile-2"));
                // channel a
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2014/03/channela-banner.jpg", "https://aqstream.com/chna/Channel-A-Stream-5"));
                // channel a plus
                //gridArr.add(new Fr1GridViewItem("http://www.channelaplus.com/images/main/gnb_left_tit.jpg", "https://aqstream.com/chnap/Channel-A-Plus-SD-Mobile"));

                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/ytn-banner.jpg", "http://www.sinktv.com/ch/korea/ytn.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/tvchosun-banner.jpg", "http://www.sinktv.com/ch/korea/tvchosun.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/yonhapnews-banner.jpg", "http://www.sinktv.com/ch/korea/yonhapnews.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/mbn-banner.jpg", "http://www.sinktv.com/ch/korea/mbn.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/ebs1-banner-1.jpg", "http://www.sinktv.com/ch/korea/ebs1.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/ebs2-banner.jpg", "http://www.sinktv.com/ch/korea/ebs2.php"));

                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/playy-banner.jpg", "http://www.sinktv.com/ch/korea/playy.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/themovie-banner.jpg", "http://www.sinktv.com/ch/korea/themovie.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/cj-banner.jpg", "http://www.sinktv.com/ch/korea/cj.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/gs-banner.jpg", "http://www.sinktv.com/ch/korea/gs.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/lotte-banner.jpg", "http://www.sinktv.com/ch/korea/lotte.php"));
                gridArr.add(new Fr1GridViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/hyundai-banner.jpg", "http://www.sinktv.com/ch/korea/hyundai.php"));



                gridView = (GridView)view.findViewById(R.id.fr1_gridview);
                adapter = new Fr1GridViewAdapter(getActivity(), gridArr, R.layout.fr1_gridview_item);
                gridView.setAdapter(adapter);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.d(" clicked - ", " gridview image ");

                        if(NetworkUtil.getConnectivity(getActivity())){
                            Intent intent = new Intent(getActivity(), VideoViewActivity.class);
                            intent.putExtra("finalUrl", gridArr.get(position).getUrl());
                            intent.putExtra("tag", "1");
                            startActivity(intent);
                            //Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(gridArr.get(position).getUrl()));
                            //startActivity(intent);
                        } else {
                            Toast.makeText(getActivity(), "check your network connection status", Toast.LENGTH_SHORT).show();
                        }


                    }
                });
            } else {
                Toast.makeText(getActivity(), "check your network connection status", Toast.LENGTH_SHORT).show();
            }


        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }



    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
