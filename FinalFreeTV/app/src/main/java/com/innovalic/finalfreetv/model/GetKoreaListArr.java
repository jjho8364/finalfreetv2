package com.innovalic.finalfreetv.model;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by jjho on 2016-08-21.
 */
public class GetKoreaListArr {
    private String TAG = "GetKoreaListArr - ";
    ArrayList<ListViewItem2> listArr;


    public ArrayList<ListViewItem2> getListArr(String title){
        ArrayList<ListViewItem2> listArr = new ArrayList<ListViewItem2>();

        Log.d(TAG, title);
        switch (title){
            case "커피프린스 1호점" :
                listArr.add(new ListViewItem2("커피프린스 1호점 - 01화", "https://cs507606.vk.me/u169686852/videos/81a562f3ab.360.mp4?extra=imAJAczs1ftqXptidRNgh8_Ssq4TW6e2T2Cjq4DJNFtIvxS1tbDCuY16WnhS33uY3ncuer7Pb0rD4kcfZozQYWRJBLpfmEpNhdUoz1y5w5hQABfCp-E44byDP_iM"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 02화", "https://cs506508.vk.me/u169686852/videos/c829fa042a.360.mp4?extra=Sv6tn_-HlF4NqHp31gdB0k2CzZepwVqFs8eavtHX2PQOUPXLUCPhLgfV2vz5L9deIaekzvcddUvtm2GhbjUgXBPs3xzhKwWla2oAmqJKlyDs4mlyQGVC3SeNKdwv"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 03화", "https://cs509510.vk.me/u169686852/videos/a9584b56da.360.mp4?extra=zVSwqY3-YGhkE_P4_m9VwCvtA2Upg4NgpovqKn-w2biD5K8gUwe7pSYXCnAhVGnJ1Ef7DDyQoruWIh4_t_MpZsmjGI2hygkEuNyWsraPrR6gvt0nslxGNpRDq86j"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 04화", "https://cs505601.vk.me/u169686852/videos/6d7d32aa83.360.mp4?extra=PTTgkQDg_eMKkQgf97ZkIV1ObJiJOwbI9ILq8kQUOyn4mZ_xNbNnfNgLw2fr8_KCv52o15jy3Q5c0TKTT04un8vw0e758pVRo5vCeHkwe28qROM9v4IyzXyvv1fJ"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 05화", "https://cs505419.vk.me/u169686852/videos/55830f238c.360.mp4?extra=iFBUHb-_Qm6op_3NwJrfYKl9RdE6_YPfOkVu3-uUk_7jRi63qfQtUALpTCbTYZTY6dbSeEJNhz50fT17mNfGV9ahaL95dLqRa_e_qLtCBBy1d3qQ-GMsf55Ueeur"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 06화", "https://cs506420.vk.me/u169686852/videos/328c940239.360.mp4?extra=aaYRkIrdqOJyp8LOQmJ5IUErxlZAdHnswJtK2yNuP4ZgqMBssPSQtWtDEnQzGCJHob7NmV2FqmQRuRwDgjFW2x4IAiD3ebSDANK4VbxVMxi-g26sev8XMXutmZIm"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 07화", "https://cs506405.vk.me/u169686852/videos/55226da6a0.360.mp4?extra=6TBbsjgSTJQm-qbpLC_EaaXyawZC98Z8hj2ZyaR8pjsf6DjGDQCsCSNHDu3dB9sLzgatkapAA9GiQSjR1YJxlbb3_WMpCGGucnNYKVs8x3nySyyGNhulOGPGIriE"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 08화", "https://cs504519.vk.me/u169686852/videos/59b1c3ee07.360.mp4?extra=U9tdt2K9UAYNdEmOn-swV8wlySv0PQOPHLQlL-TEm_A1JAjvI00Ct1fkBAU-Ey0VR2egdWEvbxwcB509iTNIAFD3QvInZpgiJx4xudYGpME-sT8tqE8Bm-fCMKv6"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 09화", "https://cs506509.vk.me/u169686852/videos/e8fa0a685e.360.mp4?extra=AznaVP4t2ks-tP1bKV0bl765rO9IfJExrfdkfh-6dAbNmRvjTLJ0C2OWbhpNQxQfy7Q2aEGtkp_4btnNFXIrhdQNT-QYMQ5UBhC-xRs5cvjB3ZR0byre2e9WfyJZ"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 10화", "https://cs507621.vk.me/u169686852/videos/2ec7a6a8e7.360.mp4?extra=O0SDnoFoEgEZVhKjGZY1kGDenQvKOKK_wFqaKTLHXaqww6xlIPLjSx5GFiSVzbQMpbfcaH7EJv9wK0poNPO59jUBYaVPJLOCwMVHD2OLggIWmL-BPXS5kErUhr7T"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 11화", "https://cs519608.vk.me/u218132965/videos/8091a48135.360.mp4?extra=S1MnxH2b3SsQ3R0p0HoLezFnmyipjECpuD6oCcjsXGzAVDYOKYGLhWdIHCJFn5yq1krvljHpYHrM8TVDKRb2EAiPg8kZeaUR9LQDA1aZstaWS3Lt6xByiVcJyDJS"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 12화", "https://cs504523.vk.me/u169686852/videos/9de9c087d7.360.mp4?extra=47UXsR12I8sVzk3jJSAVOZHIKoTyDCuky5ERdtrnf7ay_QCAFjctjzjsS3tWabwAXuao4somfM_XH4IPFApZ4s0UFX4A-Q548AZOwKXEQYZNXS3VPbYAVGB-jzG6"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 13화", "https://cs506219.vk.me/u169686852/videos/eb6c7728d6.360.mp4?extra=SclLyq-K8PqXm-RVOmQBMmWXokfeBr-8dLeY1xBxH-5H0uHSUCNEOGYCAzobLE_UtDelyqmD-EZ25s0-g2hP5l-vYpKwCJlXkZO_sazY5MYDLNq5qxDe1e8zPrrv"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 14화", "https://cs510123.vk.me/u169686852/videos/c89bf4bf60.360.mp4?extra=S8NxJr59wyWWhEYDqM5dlMFK7y3obI5bImnNZkD0mSCgi8LYSo1Nh3terfg53mgnn2ay6g9IIYGQQBIDwXMNnV9o4GV3jdXNEzoZwMHhBbhexEbu2itVSCs3Nw5J"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 15화", "https://cs507518.vk.me/u169686852/videos/8fae7079d9.360.mp4?extra=9GN7yC_z8qBxJcgl0Ugf7B8g17qn1EJZJ42jnzMpEnUCaVYeCrrx1iDf3WiMgQFOrmsvt9wRO4dkxopjPFCo-HZnhRjP6Z1e9g-jKpc2q9YvjIABsFfwdAt16aqk"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 16화", "https://cs507505.vk.me/u169686852/videos/7f821fca11.360.mp4?extra=HR46_uiLJjjD3NzSEXGJBd8Pdnium5XiCe04U0Seqs2FrHfFfMQiuvPpU18QRxw1BrdmtUfjBoZPML_Y5fq847Aw9g6mC4hyPvKBMwXHozlUol8A0s6_3gBBri-a"));
                listArr.add(new ListViewItem2("커피프린스 1호점 - 17화 완결", "https://cs507208.vk.me/u169686852/videos/d9a2a75f8a.360.mp4?extra=7nQlTleqlFPpcNJ6U0cIMAT7fBMrnl2RvOwBtHCr5IkOZOpgP4NOAcjTRtT15X5jEn88s1SoLrJjMPSlbTT5hZou7ltK8brev_dhWK8E7mY-efkzcZvx1frUkkyJ"));
                break;


        }
        return listArr;
    }
}
