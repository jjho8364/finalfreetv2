package com.innovalic.finalfreetv.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.innovalic.finalfreetv.R;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class VideoListActivity extends Activity implements View.OnClickListener {

    private final String TAG = " VideoListActivity -  ";
    private ProgressDialog mProgressDialog;
    private String listUrl = "";
    private String imgUrl = "";
    private String title = "";
    private ImageView imageView;
    private TextView tv_title;
    private ArrayList<String> textArr;
    private ArrayList<String> linkUrlArr;
    private ArrayList<Button> bbb;
    private Button btn01;
    private Button btn02;

    private String oneUrl="";

    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video_list);

        AdView mAdView1 = (AdView) findViewById(R.id.adView_1);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView1.loadAd(adRequest);

        Intent intent = getIntent();
        listUrl = (String)intent.getSerializableExtra("listUrl");
        imgUrl = (String)intent.getSerializableExtra("imgUrl");
        title = (String)intent.getSerializableExtra("title");

        imageView = (ImageView)findViewById(R.id.videolist_img);
        tv_title = (TextView)findViewById(R.id.videolist_title);
        Picasso.with(this).load(imgUrl).into(imageView);
        tv_title.setText(title);

        linearLayout = (LinearLayout)findViewById(R.id.li_linklist);
        btn01 = (Button)findViewById(R.id.videolist_btn01);
        btn02 = (Button)findViewById(R.id.videolist_btn02);
        btn01.setOnClickListener(this);
        btn02.setOnClickListener(this);

        bbb = new ArrayList<Button>();

        // 링크 주소 가져오기
        GetLinkAddr getLinkAddr = new GetLinkAddr();
        getLinkAddr.execute();

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(VideoListActivity.this, VideoViewActivity.class);
        switch (v.getId()){
            case R.id.videolist_btn01 :
                if(linkUrlArr==null || linkUrlArr.size()<1){
                    //Log.d(TAG, "");
                    //Toast.makeText(VideoListActivity.this, "방송정보가 없습니다.", Toast.LENGTH_LONG).show();
                    intent = new Intent(VideoListActivity.this, VideoViewActivity.class);
                    intent.putExtra("finalUrl", listUrl);
                    intent.putExtra("tag", "0");
                    //Log.d(TAG, "oneUrl : " + listUrl);
                    startActivity(intent);

                } else {
                    //Log.d(TAG, linkUrlArr.get(0));
                    intent = new Intent(VideoListActivity.this, VideoViewActivity.class);
                    intent.putExtra("finalUrl", linkUrlArr.get(0));
                    intent.putExtra("tag", "0");
                    startActivity(intent);
                }
                break;
            case R.id.videolist_btn02 :
                if(linkUrlArr==null || linkUrlArr.size()<2){
                    //Toast.makeText(VideoListActivity.this, "방송정보가 없습니다.", Toast.LENGTH_LONG).show();
                    intent = new Intent(VideoListActivity.this, VideoViewActivity.class);
                    intent.putExtra("finalUrl", listUrl);
                    intent.putExtra("tag", "0");
                    //Log.d(TAG, "oneUrl : " + listUrl);
                    startActivity(intent);
                } else {
                    Log.d(TAG, linkUrlArr.get(1));
                    intent = new Intent(VideoListActivity.this, VideoViewActivity.class);
                    intent.putExtra("finalUrl", linkUrlArr.get(1));
                    intent.putExtra("tag", "0");
                    startActivity(intent);
                    break;
                }
        }
    }

    public class GetLinkAddr extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(VideoListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            textArr = new ArrayList<String>();
            linkUrlArr = new ArrayList<String>();

            Document doc = null;
            Log.d(TAG, listUrl);
            try {
                doc = Jsoup.connect(listUrl).userAgent("Chrome").timeout(10000).get();
                Elements lis = doc.select(".pagination.post-tape li");

                Elements iframe = doc.select("iframe");

                String urlAddr = iframe.attr("src");
                oneUrl = urlAddr;
                Log.d(TAG, "urlAddr : "  + urlAddr);

                for(int i=0 ; i<lis.size() ; i++){
                    textArr.add(lis.get(i).text());
                    linkUrlArr.add(lis.get(i).select("a").attr("href"));
                    Log.d(TAG, "linkUrlArr : " + lis.get(i).select("a").attr("href"));
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            mProgressDialog.dismiss();
        }
    }




}
