package com.innovalic.finalfreetv.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.innovalic.finalfreetv.R;
import com.innovalic.finalfreetv.model.GridViewItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-02.
 */
public class GridViewAdapter extends BaseAdapter {

    Activity context;
    ArrayList<GridViewItem> gridArr;
    LayoutInflater inf;
    int layout;

    static class ViewHolder {
        public ImageView imageView;
        public TextView urlPath;
        public TextView title;
        public TextView updateDt;
    }

    public GridViewAdapter(){

    }

    public GridViewAdapter(Activity context, ArrayList<GridViewItem> gridArr, int layout) {
        this.context = context;
        this.gridArr = gridArr;
        this.inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layout = layout;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inf.inflate(layout, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageView= (ImageView)rowView.findViewById(R.id.griditem_img);
            viewHolder.title = (TextView)rowView.findViewById(R.id.griditem_title);
            viewHolder.updateDt = (TextView)rowView.findViewById(R.id.griditem_updatedt);

            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder)rowView.getTag();
        GridViewItem data = gridArr.get(position);
        if(data.getImgUrl() != null && !data.getImgUrl().equals("")){
            Picasso.with(context).load(data.getImgUrl()).into(holder.imageView);
        }
        holder.title.setText(data.getTitle());
        holder.updateDt.setText(data.getUpdateDt());

        return rowView;
    }

    @Override
    public int getCount() {
        return gridArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
