package com.innovalic.finalfreetv.model;

/**
 * Created by Administrator on 2016-07-24.
 */
public class GridViewItemMid {
    private String imgUrl;
    private String title;

    public GridViewItemMid(String imgUrl, String title) {
        this.imgUrl = imgUrl;
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
