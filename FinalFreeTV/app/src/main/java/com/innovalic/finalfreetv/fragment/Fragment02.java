package com.innovalic.finalfreetv.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.innovalic.finalfreetv.R;
import com.innovalic.finalfreetv.activity.MainActivity;
import com.innovalic.finalfreetv.activity.VideoListActivity;
import com.innovalic.finalfreetv.adapter.GridViewAdapter;
import com.innovalic.finalfreetv.model.GridViewItem;
import com.innovalic.finalfreetv.utils.NetworkUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-02.
 */
public class Fragment02 extends Fragment implements View.OnClickListener {
    private final String TAG = " Fragment02 - ";
    private ProgressDialog mProgressDialog;
    //private ArrayList<GridViewItem> listArr;
    private ArrayList<GridViewItem> gridArr;
    private final String baseUrl = "http://www.marutv.com";
    private final String categoryUrl = "/entertainment/page/";
    private GridView gridView;
    private GetGridView getGridView = null;

    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        ((MainActivity)getActivity()).hideHistoryBtn(); // history hide
        int portrait_width_pixel= Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        //boolean phoneNumFlag = false;
        boolean usimFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        int contactsCnt = c.getCount();
        if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        //String phoneNum = telManager.getLine1Number();
        //if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        //if(operator == null || operator.equals("")){ operatorFlag = true; }
        if (telManager.getSimState() == TelephonyManager.SIM_STATE_ABSENT){
            Log.d(TAG, "유심 x");
            usimFlag = true;
        } else {
            Log.d(TAG, "유심 o");
            usimFlag = false;
        }

        Log.d(TAG, "contactsFlag : " + contactsFlag);
        Log.d(TAG, "usimFlag : " + usimFlag);
        Log.d(TAG, "isPhone : " + isPhone);
        //Log.d(TAG, "phoneNumFlag : " + phoneNumFlag);
        if(false){
        //if ((contactsFlag || usimFlag) && isPhone) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment01, container, false);
            gridView = (GridView)view.findViewById(R.id.gridview);

            tv_currentPage = (TextView)view.findViewById(R.id.fr01_currentpage);
            tv_lastPage = (TextView)view.findViewById(R.id.fr01_lastpage);
            preBtn = (Button)view.findViewById(R.id.fr01_prebtn);
            nextBtn = (Button)view.findViewById(R.id.fr01_nextbtn);

            preBtn.setOnClickListener(this);
            nextBtn.setOnClickListener(this);

            getGridView = new GetGridView();
            getGridView.execute();
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr01_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    //asyncGridview.execute();
                    new GetGridView().execute();
                }
                break;

            case R.id.fr01_nextbtn :
                if(pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    //asyncGridview.execute();
                    new GetGridView().execute();
                }
                break;
        }
    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        String tempLastPage = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            gridArr = null;
            gridArr = new ArrayList<GridViewItem>();

            Document doc = null;
            try {
                doc = Jsoup.connect(baseUrl + categoryUrl + pageNum).timeout(15000).userAgent("Chrome").get();
                Elements div = doc.select(".video-section div.item");
                Elements pages = doc.select(".pagination li");

                for(int i=0 ; i<div.size() ; i++){
                    String listUrl = div.get(i).select("a:nth-child(1)").attr("href");
                    String imgUrl = div.get(i).select("a:nth-child(1) img").attr("src");
                    String title = div.get(i).select("h3").text();
                    String updateDt = div.get(i).select(".meta .date").text();


                    if(pages.get(pages.size()-1).text().equals("다음 →")){
                        tempLastPage = pages.get(pages.size()-2).text();
                    } else {
                        tempLastPage = pages.get(pages.size()-1).text();
                    }

                    gridArr.add(new GridViewItem(imgUrl, title, listUrl, updateDt));
                    //Log.d(TAG, imgUrl);
                    //Log.d(TAG, title);
                    //Log.d(TAG, listUrl);
                    //Log.d(TAG, updateDt);
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            tv_currentPage.setText(pageNum+"");
            tv_lastPage.setText(tempLastPage);

            if(gridArr.size()==0){
                Toast.makeText(getActivity(), "사용자가 많습니다. 다시 시도해 주세요.", Toast.LENGTH_SHORT).show();
            }
            // adapter에 적용
            gridView.setAdapter(new GridViewAdapter(getActivity(), gridArr, R.layout.gridviewitem));

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(NetworkUtil.getConnectivity(getActivity())){
                        //String listUrl = baseUrl + gridArr.get(position).getListUrl();
                        //Log.d(TAG, listUrl);

                        Intent intent = new Intent(getActivity(), VideoListActivity.class);
                        intent.putExtra("listUrl", gridArr.get(position).getListUrl());
                        intent.putExtra("imgUrl", gridArr.get(position).getImgUrl());
                        intent.putExtra("title", gridArr.get(position).getTitle());
                        startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), "check your network connection status", Toast.LENGTH_SHORT).show();
                    }
                }
            });


            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getGridView != null){
            getGridView.cancel(true);
        }

    }
}
