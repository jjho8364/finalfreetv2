package com.innovalic.finalfreetv.model;

/**
 * Created by Administrator on 2016-07-07.
 */
public class ListViewItem2 {

    private String title;
    private String videoUrl;

    public ListViewItem2(String title, String videoUrl) {
        this.title = title;
        this.videoUrl = videoUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
