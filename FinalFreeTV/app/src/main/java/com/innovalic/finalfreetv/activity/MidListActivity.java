package com.innovalic.finalfreetv.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.innovalic.finalfreetv.R;
import com.innovalic.finalfreetv.adapter.ListViewAdapter2;
import com.innovalic.finalfreetv.model.GetKoreaListArr;
import com.innovalic.finalfreetv.model.GetMidListArr;
import com.innovalic.finalfreetv.model.GetMidListArr2;
import com.innovalic.finalfreetv.model.GetMidListArr3;
import com.innovalic.finalfreetv.model.GetMidListArr4;
import com.innovalic.finalfreetv.model.ListViewItem2;

import java.util.ArrayList;
import java.util.List;

public class MidListActivity extends Activity implements AdapterView.OnItemClickListener {
    private final String TAG = " MidListActivity -  ";
    private ProgressDialog mProgressDialog;
    String title = "";

    ListView listView2;
    ArrayList<ListViewItem2> listArr;

    String type = "";

    String[] sfArr = new String[5];

    String packageName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mid_list);

        AdView mAdViewMidUpper = (AdView) findViewById(R.id.adView_mid_upper);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdViewMidUpper.loadAd(adRequest);

        Intent intent = getIntent();
        title = (String)intent.getSerializableExtra("title");
        type = (String)intent.getSerializableExtra("type");

        listArr = new ArrayList<ListViewItem2>();
        classifyTitle(type);

        listView2 = (ListView)findViewById(R.id.listview2);

        listView2.setAdapter(new ListViewAdapter2(MidListActivity.this, getLayoutInflater(), listArr));
        listView2.setOnItemClickListener(this);


        PackageManager pm = getPackageManager();
        List<ApplicationInfo> list = pm.getInstalledApplications(0);
        for (ApplicationInfo applicationInfo : list) {
            String pName = applicationInfo.packageName;   // 앱 패키지
            if(pName.equals("com.mxtech.videoplayer.pro")){
                packageName = "com.mxtech.videoplayer.pro";
                Log.d(TAG, "pacakge : pro ");
                break;
            } else if(pName.equals("com.mxtech.videoplayer.ad")){
                packageName = "com.mxtech.videoplayer.ad";
                Log.d(TAG, "pacakge : ad ");
                break;
            }
        }

        if(packageName.equals("")){
            Toast.makeText(MidListActivity.this, "mx player를 설치하세요. 동영상 떨림 현상이 없어지고, 화질이 향상 됩니다.", Toast.LENGTH_SHORT).show();
        }

        getSfPref();
    }

    public void pushSfArr(){
        sfArr[4] = sfArr[3];
        sfArr[3] = sfArr[2];
        sfArr[2] = sfArr[1];
        sfArr[1] = sfArr[0];
    }

    public void setSfArr(){
        SharedPreferences sf = getSharedPreferences("history", MODE_PRIVATE);
        SharedPreferences.Editor editor = sf.edit();
        editor.putString("one", sfArr[0]);
        editor.putString("two", sfArr[1]);
        editor.putString("three", sfArr[2]);
        editor.putString("four", sfArr[3]);
        editor.putString("five", sfArr[4]);
        editor.commit();

    }

    public void getSfPref(){
        SharedPreferences sf = getSharedPreferences("history", MODE_PRIVATE);
        sfArr[0] = sf.getString("one", "");
        sfArr[1] = sf.getString("two", "");
        sfArr[2] = sf.getString("three", "");
        sfArr[3] = sf.getString("four", "");
        sfArr[4] = sf.getString("five", "");
    }

    public void classifyTitle(String type){
        Log.d(TAG, title);
        GetMidListArr getMidListArr = new GetMidListArr();
        GetKoreaListArr getKoreaListArr = new GetKoreaListArr();
        //GetJapanListArr getJapanListArr = new GetJapanListArr();
        //GetEnglandListArr getEnglandListArr = new GetEnglandListArr();
        switch (type){
            case "mid" :
                listArr = getMidListArr.getListArr(title);
                if(listArr == null || listArr.size() == 0){
                    GetMidListArr2 getMidListArr2 = new GetMidListArr2();
                    listArr = getMidListArr2.getListArr(title);
                }
                if(listArr == null || listArr.size() == 0) {
                    GetMidListArr3 getMidListArr3 = new GetMidListArr3();
                    listArr = getMidListArr3.getListArr(title);
                }
                if(listArr == null || listArr.size() == 0) {
                    GetMidListArr4 getMidListArr4 = new GetMidListArr4();
                    listArr = getMidListArr4.getListArr(title);
                }
                break;
            case "kr" :
                listArr = getKoreaListArr.getListArr(title);
                break;
            /*case "ild" :
                listArr = getJapanListArr.getListArr(title);
                break;
            case "youngd" :
                listArr = getEnglandListArr.getListArr(title);
                break;*/
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        pushSfArr();
        sfArr[0] = listArr.get(position).getTitle();
        setSfArr();

        if(listArr.get(position).getVideoUrl().contains("mgapi.wecandeo.com") || listArr.get(position).getVideoUrl().contains("videofarm.daum") || listArr.get(position).getVideoUrl().contains("d1wst0behutosd") || listArr.get(position).getVideoUrl().contains("serviceapi") || listArr.get(position).getVideoUrl().contains("blog.naver.com")){
            Intent intent = new Intent(MidListActivity.this, VideoViewActivity.class);
            intent.putExtra("finalUrl", listArr.get(position).getVideoUrl());
            intent.putExtra("tag", "1");
            startActivity(intent);
        } else {
            // 패키지 검사후 mx 플레이어가 있으면
            if(packageName.equals("")){
                Intent intent = new Intent(MidListActivity.this, VideoViewActivity.class);
                intent.putExtra("finalUrl", listArr.get(position).getVideoUrl());
                intent.putExtra("tag", "1");
                startActivity(intent);
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri videoUri = Uri.parse(listArr.get(position).getVideoUrl());
                intent.setDataAndType( videoUri, "application/x-mpegURL" );
                intent.putExtra("decode_mode", (byte)2);
                intent.setPackage(packageName);
                startActivity(intent);
            }
        }


        /*Intent intent = new Intent(MidListActivity.this, VideoViewActivity.class);
        intent.putExtra("finalUrl", listArr.get(position).getVideoUrl());
        intent.putExtra("tag", "1");
        startActivity(intent);*/
    }
}
