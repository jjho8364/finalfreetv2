package com.innovalic.finalfreetv.model;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by jjho on 2016-09-25.
 */
public class GetMidListArr4 {
    private String TAG = "GetMidListArr4 - ";
    ArrayList<ListViewItem2> listArr;


    public ArrayList<ListViewItem2> getListArr(String title){
        ArrayList<ListViewItem2> listArr = new ArrayList<ListViewItem2>();

        Log.d(TAG, title);
        switch (title){
            case "본즈 시즌1" :
                listArr.add(new ListViewItem2("본즈 시즌1 - 01화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/23/1540709.flv?px-bps=1486765&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 02화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/23/1540724.flv?px-bps=1450395&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 03화", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/23/1540896.flv?px-bps=1453503&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 04화", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/23/1541640.flv?px-bps=1461457&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 05화", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/24/1541910.flv?px-bps=1402461&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 06화", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/24/1541957.flv?px-bps=1406206&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 07화", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/24/1542017.flv?px-bps=1005426&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 08화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/24/1542043.flv?px-bps=1434982&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 09화", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/24/1542056.flv?px-bps=1476297&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 10화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/24/1542061.flv?px-bps=1440624&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 11화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/24/1542074.flv?px-bps=1433094&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 12화", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/24/1542093.flv?px-bps=1449907&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 13화", "http://cmvs.mgoon.com/storage4/m2_video/2008/05/24/1542139.flv?px-bps=1485144&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 14화", "http://cmvs.mgoon.com/storage3/m2_video/2008/05/26/1544086.flv?px-bps=1435741&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 15화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/26/1544127.flv?px-bps=1434598&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 16화", "http://cmvs.mgoon.com/storage3/m2_video/2008/05/26/1545621.flv?px-bps=1435627&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 17화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/26/1545802.flv?px-bps=1458996&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 18화", "http://cmvs.mgoon.com/storage4/m2_video/2008/05/27/1546047.flv?px-bps=1453857&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 19화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/27/1546117.flv?px-bps=1435275&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 20화", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/27/1546134.flv?px-bps=1433449&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 21화", "http://cmvs.mgoon.com/storage3/m2_video/2008/05/27/1546175.flv?px-bps=1435536&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌1 - 22화 완결", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/27/1546236.flv?px-bps=1402743&px-bufahead=10"));
                break;
            case "본즈 시즌2" :
                listArr.add(new ListViewItem2("본즈 시즌2 - 01화", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/28/1548873.flv?px-bps=1402462&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 02화", "http://cmvs.mgoon.com/storage4/m2_video/2008/05/28/1548956.flv?px-bps=1457751&px-bufahead=105"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 03화", "http://cmvs.mgoon.com/storage3/m2_video/2008/05/28/1549065.flv?px-bps=1443345&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 04화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/28/1549117.flv?px-bps=967917&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 05화", "http://cmvs.mgoon.com/storage4/m2_video/2008/05/28/1549203.flv?px-bps=1396560&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 06화", "http://cmvs.mgoon.com/storage4/m2_video/2008/05/29/1549559.flv?px-bps=1434567&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 07화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/29/1549573.flv?px-bps=1434585&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 08화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/29/1549590.flv?px-bps=1482217&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 09화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/29/1549643.flv?px-bps=1393810&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 10화", "http://cmvs.mgoon.com/storage4/m2_video/2008/05/29/1549668.flv?px-bps=1394253&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 11화", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/29/1549691.flv?px-bps=1407511&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 12화", "http://cmvs.mgoon.com/storage3/m2_video/2008/05/29/1549724.flv?px-bps=1407547&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 13화", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/29/1549743.flv?px-bps=1402296&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 14화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/29/1549761.flv?px-bps=1404373&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 15화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/29/1549936.flv?px-bps=1435237&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 16화", "http://cmvs.mgoon.com/storage3/m2_video/2008/05/29/1549997.flv?px-bps=1439608&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 17화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/29/1550030.flv?px-bps=1437364&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 18화", "http://cmvs.mgoon.com/storage3/m2_video/2008/05/29/1550089.flv?px-bps=1434696&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 19화", "http://cmvs.mgoon.com/storage4/m2_video/2008/05/29/1550176.flv?px-bps=1440502&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 20화", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/29/1550180.flv?px-bps=1404636&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌2 - 21화 완결", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/29/1550277.flv?px-bps=1425582&px-bufahead=10"));
                break;
            case "본즈 시즌3" :
                listArr.add(new ListViewItem2("본즈 시즌3 - 01화", "http://cmvs.mgoon.com/storage1/m2_video/2008/05/31/1553349.flv?px-bps=1380250&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌3 - 02화", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/31/1553505.flv?px-bps=1446300&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌3 - 03화", "http://cmvs.mgoon.com/storage4/m2_video/2008/05/31/1553513.flv?px-bps=1401378&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌3 - 04화", "http://cmvs.mgoon.com/storage3/m2_video/2008/05/31/1553576.flv?px-bps=1361257&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌3 - 05화", "http://cmvs.mgoon.com/storage2/m2_video/2008/05/31/1553649.flv?px-bps=1406980&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌3 - 06화", "http://cmvs.mgoon.com/storage3/m2_video/2008/05/31/1553810.flv?px-bps=1449927&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌3 - 07화", "http://cmvs.mgoon.com/storage4/m2_video/2008/06/01/1553928.flv?px-bps=1436673&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌3 - 08화", "http://cmvs.mgoon.com/storage4/m2_video/2008/06/01/1554055.flv?px-bps=1464114&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌3 - 09화", "http://cmvs.mgoon.com/storage3/m2_video/2008/06/01/1554402.flv?px-bps=1446631&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌3 - 10화", "http://cmvs.mgoon.com/storage2/m2_video/2008/06/01/1554471.flv?px-bps=1456105&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌3 - 11화", "http://cmvs.mgoon.com/storage2/m2_video/2008/06/01/1554553.flv?px-bps=1440939&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌3 - 12화", "http://cmvs.mgoon.com/storage1/m2_video/2008/06/01/1554564.flv?px-bps=1405848&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌3 - 13화", "http://cmvs.mgoon.com/storage4/m2_video/2008/06/01/1554637.flv?px-bps=1433872&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌3 - 14화", "http://cmvs.mgoon.com/storage1/m2_video/2008/06/01/1555219.flv?px-bps=1432176&px-bufahead=10"));
                listArr.add(new ListViewItem2("본즈 시즌3 - 15화 완결", "http://cmvs.mgoon.com/storage4/m2_video/2008/06/01/1555327.flv?px-bps=1399005&px-bufahead=10"));
                break;
            case "소프라노스 시즌1" :
                listArr.add(new ListViewItem2("소프라노스 시즌1 - 01화", "http://cmvs.mgoon.com/storage1/m2_video/2010/06/03/3242288.mp4?px-bps=1372306&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌1 - 02화", "http://cmvs.mgoon.com/storage1/m2_video/2010/06/03/3242316.mp4?px-bps=1360282&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌1 - 03화", "http://cmvs.mgoon.com/storage1/m2_video/2010/06/03/3242322.mp4?px-bps=1361452&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌1 - 04화", "http://cmvs.mgoon.com/storage4/m2_video/2010/06/03/3242290.mp4?px-bps=1351485&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌1 - 05화", "http://cmvs.mgoon.com/storage1/m2_video/2010/06/03/3242385.mp4?px-bps=1352161&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌1 - 06화", "http://cmvs.mgoon.com/storage4/m2_video/2010/06/03/3242405.mp4?px-bps=1362355&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌1 - 07화", "http://cmvs.mgoon.com/storage4/m2_video/2010/06/03/3242438.mp4?px-bps=1360566&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌1 - 08화", "http://cmvs.mgoon.com/storage4/m2_video/2010/06/03/3242436.mp4?px-bps=1355439&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌1 - 09화", "http://cmvs.mgoon.com/storage4/m2_video/2010/06/03/3242439.mp4?px-bps=1355368&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌1 - 10화", "http://cmvs.mgoon.com/storage1/m2_video/2010/06/03/3242485.mp4?px-bps=1364649&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌1 - 11화", "http://cmvs.mgoon.com/storage2/m2_video/2010/06/03/3242458.mp4?px-bps=1370563&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌1 - 12화", "http://cmvs.mgoon.com/storage1/m2_video/2010/06/03/3242503.mp4?px-bps=1364017&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌1 - 13화 완결", "http://cmvs.mgoon.com/storage2/m2_video/2010/06/03/3242499.mp4?px-bps=1363794&px-bufahead=10"));
                break;
            case "소프라노스 시즌2" :
                listArr.add(new ListViewItem2("소프라노스 시즌2 - 01화", "http://cmvs.mgoon.com/storage4/m2_video/2010/06/03/3242500.mp4?px-bps=1366509&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌2 - 02화", "http://cmvs.mgoon.com/storage4/m2_video/2010/06/03/3242541.mp4?px-bps=1366369&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌2 - 03화", "http://cmvs.mgoon.com/storage4/m2_video/2010/06/03/3242694.mp4?px-bps=1372929&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌2 - 04화", "http://cmvs.mgoon.com/storage4/m2_video/2010/06/03/3242731.mp4?px-bps=1369975&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌2 - 05화", "http://cmvs.mgoon.com/storage4/m2_video/2010/06/03/3242877.mp4?px-bps=1360147&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌2 - 06화", "http://cmvs.mgoon.com/storage3/m2_video/2010/06/03/3242718.mp4?px-bps=1363050&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌2 - 07화", "http://cmvs.mgoon.com/storage2/m2_video/2010/06/03/3242779.mp4?px-bps=1358665&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌2 - 08화", "http://cmvs.mgoon.com/storage2/m2_video/2010/06/03/3242926.mp4?px-bps=1365559&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌2 - 09화", "http://cmvs.mgoon.com/storage2/m2_video/2010/06/03/3242750.mp4?px-bps=1367709&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌2 - 10화", "http://cmvs.mgoon.com/storage3/m2_video/2010/06/03/3242838.mp4?px-bps=1370628&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌2 - 11화", "http://cmvs.mgoon.com/storage1/m2_video/2010/06/03/3242795.mp4?px-bps=1369744&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌2 - 12화", "http://cmvs.mgoon.com/storage1/m2_video/2010/06/03/3242956.mp4?px-bps=1359571&px-bufahead=10"));
                listArr.add(new ListViewItem2("소프라노스 시즌2 - 13화 완결", "http://cmvs.mgoon.com/storage2/m2_video/2010/06/03/3242975.mp4?px-bps=1364595&px-bufahead=10"));
                break;
            case "소프라노스 시즌3" :
                listArr.add(new ListViewItem2("소프라노스 시즌3 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=qleBgz1Qq0A$"));
                listArr.add(new ListViewItem2("소프라노스 시즌3 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=TTSwANjY3R8$"));
                listArr.add(new ListViewItem2("소프라노스 시즌3 - 03화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=cdqNF-PUHuI$"));
                listArr.add(new ListViewItem2("소프라노스 시즌3 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=YE7IchgG5aQ$"));
                listArr.add(new ListViewItem2("소프라노스 시즌3 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=H9axNUeDbZs$"));
                listArr.add(new ListViewItem2("소프라노스 시즌3 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=FyTKm9TDYjw$"));
                listArr.add(new ListViewItem2("소프라노스 시즌3 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=lqhyOcP1D9A$"));
                listArr.add(new ListViewItem2("소프라노스 시즌3 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=jWc3lqSv29k$"));
                listArr.add(new ListViewItem2("소프라노스 시즌3 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=rXT4gUdLHPY$"));
                listArr.add(new ListViewItem2("소프라노스 시즌3 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=FzBzvyiG0jY$"));
                listArr.add(new ListViewItem2("소프라노스 시즌3 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=c8QbiPIR3n0$"));
                listArr.add(new ListViewItem2("소프라노스 시즌3 - 12화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=svqDB9p96nk$"));
                listArr.add(new ListViewItem2("소프라노스 시즌3 - 13화 완결", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=M4zxgjPw5lM$"));
                break;
            case "카프리카 시즌1" :
                listArr.add(new ListViewItem2("카프리카 시즌1 - 01화", "http://cmvs.mgoon.com/storage3/m2_video/2010/01/24/2883280.mp4?px-bps=1357510&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 02화", "http://cmvs.mgoon.com/storage3/m2_video/2010/01/31/2905272.mp4?px-bps=1362592&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 03화", "http://cmvs.mgoon.com/storage2/m2_video/2010/02/08/2929606.mp4?px-bps=1350994&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 04화", "http://cmvs.mgoon.com/storage3/m2_video/2010/02/22/2971090.mp4?px-bps=1348020&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 05화", "http://cmvs.mgoon.com/storage2/m2_video/2010/03/02/2994694.mp4?px-bps=1350717&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 06화", "http://cmvs.mgoon.com/storage2/m2_video/2010/03/09/3014781.mp4?px-bps=1344853&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 07화", "http://cmvs.mgoon.com/storage2/m2_video/2010/03/15/3031738.mp4?px-bps=1346703&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 08화", "http://cmvs.mgoon.com/storage4/m2_video/2010/03/22/3049631.mp4?px-bps=1342051&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 09화", "http://cmvs.mgoon.com/storage1/m2_video/2010/03/30/3071545.mp4?px-bps=1359351&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 10화", "http://cmvs.mgoon.com/storage1/m2_video/2010/10/09/4168183.mp4?px-bps=1398717&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 11화", "http://cmvs.mgoon.com/storage4/m2_video/2010/10/16/4179727.mp4?px-bps=1398916&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 12화", "http://cmvs.mgoon.com/storage3/m2_video/2010/10/22/4190282.mp4?px-bps=1396294&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 13화", "http://cmvs.mgoon.com/storage2/m2_video/2010/11/12/4224642.mp4?px-bps=1403257&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 14화", "http://cmvs.mgoon.com/storage3/m2_video/2010/11/15/4230048.mp4?px-bps=1395553&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 15화", "http://cmvs.mgoon.com/storage2/m2_video/2010/11/19/4236693.mp4?px-bps=1397796&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 16화", "http://cmvs.mgoon.com/storage2/m2_video/2010/11/28/4249960.mp4?px-bps=1292076&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 17화", "http://cmvs.mgoon.com/storage3/m2_video/2010/12/01/4254410.mp4?px-bps=1402464&px-bufahead=10"));
                listArr.add(new ListViewItem2("카프리카 시즌1 - 18화 완결", "http://cmvs.mgoon.com/storage3/m2_video/2010/12/05/4259481.mp4?px-bps=1391008&px-bufahead=10"));
                break;
            /*case "스콜피온 시즌1" :
                listArr.add(new ListViewItem2("콜로니 시즌1 - 01화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 02화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 03화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 04화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 05화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 06화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 07화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 08화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 09화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 10화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 11화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 12화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 13화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 14화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 15화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 16화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 17화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 18화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 19화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 20화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 21화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 22화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 23화", ""));
                listArr.add(new ListViewItem2("콜로니 시즌1 - 24화", ""));
                break;*/

        }
        return listArr;
    }
}
