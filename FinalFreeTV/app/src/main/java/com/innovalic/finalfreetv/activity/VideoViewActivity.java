package com.innovalic.finalfreetv.activity;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;


import com.innovalic.finalfreetv.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class VideoViewActivity extends Activity {
    private final String TAG = " VideoViewActivity - ";
    private ProgressDialog mProgressDialog;
    private WebView webView;
    private FrameLayout customViewContainer;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private View mCustomView;
    private myWebChromeClient mWebChromeClient;
    private myWebViewClient mWebViewClient;

    private String finalUrl;
    private String tag;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_videoview);


        tag = "0";
        Intent intent = getIntent();
        finalUrl = (String)intent.getSerializableExtra("finalUrl");
        if( intent.getSerializableExtra("tag") == null || !intent.getSerializableExtra("tag").equals("")){
            tag = (String)intent.getSerializableExtra("tag");
        }

        customViewContainer = (FrameLayout) findViewById(R.id.customViewContainer);
        webView = (WebView) findViewById(R.id.webView);

        webView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                Log.d(TAG, "download start");
                DownloadManager.Request request = new DownloadManager.Request( Uri.parse(url));
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "download");
                DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                dm.enqueue(request);

            }
        });

        mWebViewClient = new myWebViewClient();
        webView.setWebViewClient(mWebViewClient);

        mWebChromeClient = new myWebChromeClient();
        webView.setWebChromeClient(mWebChromeClient);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setSaveFormData(true);

        //Log.d(TAG, finalUrl);
        if(tag.equals("1")){
            Toast.makeText(VideoViewActivity.this, "화면이 떨리면서 멈출 경우 전체화면 버튼을 누르세요.", Toast.LENGTH_SHORT).show();
            webView.loadUrl(finalUrl);
        } else {
            new GetVideoAddr().execute();
        }

    }

    public boolean inCustomView() {
        return (mCustomView != null);
    }

    public void hideCustomView() {
        mWebChromeClient.onHideCustomView();
    }

    @Override
    protected void onPause() {
        super.onPause();    //To change body of overridden methods use File | Settings | File Templates.
        webView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();    //To change body of overridden methods use File | Settings | File Templates.
        webView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();    //To change body of overridden methods use File | Settings | File Templates.
        if (inCustomView()) {
            hideCustomView();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if (inCustomView()) {
                hideCustomView();
                return true;
            }

            if ((mCustomView == null) && webView.canGoBack()) {
                webView.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    class myWebChromeClient extends WebChromeClient {
        private Bitmap mDefaultVideoPoster;
        private View mVideoProgressView;

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);    //To change body of overridden methods use File | Settings | File Templates.
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {

            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mCustomView = view;
            webView.setVisibility(View.GONE);
            customViewContainer.setVisibility(View.VISIBLE);
            customViewContainer.addView(view);
            customViewCallback = callback;
        }

        @Override
        public View getVideoLoadingProgressView() {

            if (mVideoProgressView == null) {
                LayoutInflater inflater = LayoutInflater.from(VideoViewActivity.this);
                mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();    //To change body of overridden methods use File | Settings | File Templates.
            if (mCustomView == null)
                return;

            webView.setVisibility(View.VISIBLE);
            customViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            customViewContainer.removeView(mCustomView);
            customViewCallback.onCustomViewHidden();

            mCustomView = null;
        }
    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);    //To change body of overridden methods use File | Settings | File Templates.
        }
    }

    public class GetVideoAddr extends AsyncTask<Void, Void, Void> {

        String urlPath = "";
        boolean isDuddo = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(VideoViewActivity.this);
            mProgressDialog.setTitle("방송을 불러오고 있습니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;
            try {
                doc = Jsoup.connect(finalUrl).userAgent("Chrome").timeout(10000).get();
                Elements path = doc.select("iframe");
                urlPath = path.attr("src");

                if( urlPath==null || urlPath.equals("")){
                    Log.d(TAG, " null or noString in");
                    String temp  = doc.select(".player").select("embed").attr("src");
                    //urlPath = temp;
                    //isDuddo = true;
                    path = doc.select(".flowplayer source");
                    urlPath = path.attr("src");
                    //Log.d(TAG, "urlPath :  "  + urlPath);
                }

                if(urlPath==null || urlPath.equals("")){
                    Elements test = doc.select(".flowplayer");
                    String url = test.select("source").attr("src");
                    urlPath = url;
                }

                /*if(urlPath==null || urlPath.equals("")){
                    Elements test = doc.select(".flowplayer");
                    String url = test.select("source").attr("src");
                    urlPath = url;
                }*/


            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mProgressDialog.dismiss();
            Log.d(TAG, "urlPath : " + urlPath);
            if(urlPath.contains("clickmon")){
                Toast.makeText(VideoViewActivity.this, "준비중인 컨테츠입니다. 잠시후 이용해주시기 바랍니다. ", Toast.LENGTH_LONG).show();
            } else {
                if(urlPath.contains("videoembed")){
                    urlPath = "https:" + urlPath;
                    String newUA= "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
                    webView.getSettings().setUserAgentString(newUA);
                    Toast.makeText(VideoViewActivity.this, "화면 우측상단 Link 버튼 클릭 후, 화면 중앙 Copy link 버튼을 눌러야 재생이 됩니다. ", Toast.LENGTH_LONG).show();
                } else if(!urlPath.contains("http")){
                    urlPath = "http://www.marutv.com/" + urlPath;
                }
                if(urlPath.equals("http://www.marutv.com/")){
                    Toast.makeText(VideoViewActivity.this, "죄송합니다. tudou 방송은 모바일에서 불러올 수 없습니다.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(VideoViewActivity.this, "화면이 떨리면서 멈출 경우 전체화면 버튼을 누르세요.", Toast.LENGTH_SHORT).show();
                }
            /*if(isDuddo){
                Toast.makeText(VideoViewActivity.this, "죄송합니다. tudou 방송은 불러올 수 없습니다.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(VideoViewActivity.this, "화면이 떨리면서 멈출 경우 전체화면 버튼을 누르세요.", Toast.LENGTH_SHORT).show();
            }*/
                Log.d(TAG, "final : " + urlPath);



                webView.loadUrl(urlPath);
            }

        }
    }


}